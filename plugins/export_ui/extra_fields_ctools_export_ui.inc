<?php
/**
 * @file
 * Define the ctools export UI for Extra Fields.
 */

$path = drupal_get_path('module', 'extra_fields');

$plugin = array(
  'name' => 'extra_field_export_ui',
  'schema' => 'extra_field',
  //'access' => 'administer extra fields',  // @todo Create new permission
  'menu' => array(
    'menu item' => 'extra_fields',
    'menu title' => 'Extra fields',
    'menu description' => 'Administer extra fields',
  ),
  'title singular' => t('extra field'),
  'title plural' => t('extra fields'),
  'title singular proper' => t('Extra field'),
  'title plural proper' => t('Extra fields'),
  //'handler' => array(
    //'class' => 'ExtraFieldsExportUI',
    //'parent' => 'ctools_export_ui',
  //),
  // Define the form wizard steps
  'use wizard' => TRUE,
  'form info' => array(
    'id' => 'extra_fields_wizard',
    'path' => "admin/structure/extra_fields/edit/%step",
    'return path' => 'admin/structure/extra_fields',
    'cancel path' => 'admin/structure/extra_fields',
    'order' => array(
      'field' => t('Field configuration'),
      'entity_bundle' => t('Enabled bundles'),
      'attachments' => t('Field attachments'),
    ),
    'forms' => array(
      'field' => array(
        'form id' => 'extra_fields_wizard_field_form',
        'include' => $path . '/plugins/export_ui/extra_fields.wizard.inc',
      ),
      'entity_bundle' => array(
        'form id' => 'extra_fields_wizard_entity_bundle_form',
        'include' => $path . '/plugins/export_ui/extra_fields.wizard.inc',
      ),
      'attachments' => array(
        'form id' => 'extra_fields_wizard_attachments_form',
        'include' => $path . '/plugins/export_ui/extra_fields.wizard.inc',
      ),
    ),
  ),
);

/**
 * Finish callback for the extra field wizard.
 */
function extra_fields_wizard_finish(&$form_state) {

}

/**
 * Cancel callback for the extra field wizard.
 */
function extra_fields_wizard_cancel(&$form_state) {

}

/**
 * Return callback for the extra field wizard.
 */
function extra_fields_wizard_return(&$form_state) {

}

/**
 * Next callback for the extra field wizard.
 */
function extra_fields_wizard_next(&$form_state) {

}
