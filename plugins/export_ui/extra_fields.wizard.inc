<?php
/**
 * @file
 * Extra field creation/edit wizard functionality.
 */

/**
 * Form callback: Form for defining field-level settings for an extra field.
 */
function extra_fields_wizard_field_form($form, &$form_state) {
  $extra_field = $form_state['item'];
  $ui = $form_state['object'];

  // Add default administrative fields
  $ui->edit_form($form, $form_state);

  // @todo Process these returned types for option display.
  $field_types = extra_fields_get_extra_field_types();
  $form['extra_field_type'] = array(
    '#type' => 'select',
    '#title' => t('Field plugin type'),
    '#options' => $field_types,
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Validation handler for the extra field field-level settings form.
 *
 * @see extra_fields_wizard_field_form().
 */
function extra_fields_wizard_field_form_validate($form, &$form_state) {

}

/**
 * Submission handler for the extra field field-level settings form.
 *
 * @see extra_fields_wizard_field_form().
 */
function extra_fields_wizard_field_form_submit($form, &$form_state) {

}

function extra_fields_wizard_entity_bundle_form($form, &$form_state) {
  $extra_field = $form_state['item'];
  $ui = $form_state['object'];

  $wrapper_defaults = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $bundle_defaults = array(
    '#type' => 'checkboxes',
    '#title' => t('Enabled bundles'),
  );

  foreach (entity_get_info() as $entity_key => $entity) {
    if ($entity['fieldable']) {
      $form[$entity_key . '_wrapper'] = array(
        '#title' => check_plain($entity['label']),
      ) + $wrapper_defaults;

      $options = array();
      foreach ($entity['bundles'] as $bundle_key => $bundle) {
        $options[$bundle_key] = $bundle['label'];
      }

      $form[$entity_key . '_wrapper']['bundles'] = array(
        '#options' => $options,
        //'#default_options' => // @todo Define default options
      ) + $bundle_defaults;
    }
  }

  return $form;
}

/**
 * Form callback: Form for defining attachment-level settings for an extra field.
 */
function extra_fields_wizard_attachments_form($form, &$form_state) {

  return $form;
}

/**
 * Validation handler for the extra field field-level settings form.
 *
 * @see extra_fields_wizard_attachments_form().
 */
function extra_fields_wizard_attachments_form_validate($form, &$form_state) {

}

/**
 * Submission handler for the extra field field-level settings form.
 *
 * @see extra_fields_wizard_attachments_form().
 */
function extra_fields_wizard_attachments_form_submit($form, &$form_state) {

}
