<?php
/**
 * @file
 * Class definitions for Extra Fields and Attachments.
 */

/**
 * @todo Add documentation.
 */
interface ExtraFieldInterface {
  /**
   * @todo Add documentation.
   */
  public function createAttachment($entity, $bundle);

  /**
   * @todo Add documentation.
   */
  public function getAttachment($entity, $bundle);

  /**
   * @todo Add documentation.
   */
  public function getAttachments();

  /**
   * @todo Add documentation.
   */
  public function getFieldEditForm();

  /**
   * @todo Add documentation.
   */
  public function getAttachmentEditForm($entity, $bundle);

  /**
   * @todo Add documentation.
   */
  public function getFormSettingsForm();

  /**
   * @todo Add documentation.
   */
  public function getViewSettingsForm();

  /**
   * @todo Add documentation.
   */
  public function getFormDisplay($entity_type, $entity_bundle, Object $entity);

  /**
   * @todo Add documentation.
   */
  public function getViewDisplay($entity_type, $entity_bundle, Object $entity);

  /**
   * @todo Add documentation.
   */
  public function getSettings();

  /**
   * @todo Add documentation.
   */
  public function save();

  /**
   * @todo Add documentation.
   */
  public function delete();
}

/**
 * @todo Add documentation.
 */
interface ExtraFieldAttachmentInterface {
  /**
   * @todo Add documentation.
   */
  public function __construct($data);

  /**
   * @todo Add documentation.
   */
  public function getFormSettingsForm();

  /**
   * @todo Add documentation.
   */
  public function getViewSettingsForm();

  /**
   * @todo Add documentation.
   */
  public function getFormDisplay(Object $entity);

  /**
   * @todo Add documentation.
   */
  public function getViewDisplay(Object $entity);

  /**
   * @todo Add documentation.
   */
  public function getSettings();

  /**
   * @todo Add documentation.
   */
  public function save();

  /**
   * @todo Add documentation.
   */
  public function delete();
}

class ExtraField implements ExtraFieldInterface {
  var $name;
  var $label;
  var $description;
  var $plugin;
  var $attachments = array();
  var $settings = array();

  public function __construct($data = array()) {
    foreach ($data as $key => $val) {
      $this->$key = $val;
    }
  }

  /**
   * Create a new attachment for the given entity and bundle.
   */
  public function createAttachment($entity, $bundle) {
    // Create the new attachment
    $attachment = ctools_export_crud_new('extra_field_attachment');

    // Assign it to this field and appropriate bundle
    $attachment->setParentField($this)
      ->setBundle($entity, $bundle);

    // Add the attachment to the attachment array
    $this->attachments[$entity][$bundle] = $attachment;

    return $attachment;
  }

  /**
   * Get an attachment if it exists for the specified entity and bundle.
   */
  public function getAttachment($entity, $bundle) {
    return getAttachments($entity, $bundle);
  }

  /**
   * Retrieve all attachments or filter by entity and/or bundle.
   *
   * @param String $entity
   *   (optional)
   * @param String $bundle
   *   (optional)
   *
   * @return
   *   Array
   */
  public function getAttachments($entity = '', $bundle = '') {
    // Default to returning all attachments
    $attachments = $this->attachments;

    if ($entity == '' && $bundle == '') {
      // No filters so we return all attachments
      return $attachments;
    }
    else if ($entity != '') {
      // Filter by entity or return an empty array
      $attachments = array_key_exists($entity, $attachments) ? $attachments[$entity] : array();

      if ($bundle != '') {
        // Filter by bundle or return an empty array
        $attachments = array_key_exists($bundle, $attachments) ? $attachments[$bundle] : array();
      }
    }

    return $attachments;
  }

  public function getFieldEditForm() {}

  public function getAttachmentEditForm($entity, $bundle) {
    $attachment = getAttachment($entity, $bundle);

    $form = array();
    $form['form display'] =  $attachment->getFormSettingsForm();
    $form['view display'] =  $attachment->getViewSettingsForm();

    return $form;
  }

  public function getFormSettingsForm() {}
  public function getViewSettingsForm() {}
  /**
   * @todo Clean up method signature.
   */
  public function getFormDisplay($entity_type, $entity_bundle, Object $entity) {
    $attachment = getAttachment($entity_type, $entity_bundle);

    return $attachment->getFormDisplay($entity);
  }

  public function getViewDisplay($entity_type, $entity_bundle, Object $entity) {
    $attachment = getAttachment($entity_type, $entity_bundle);

    return $attachment->getViewDisplay($entity);
  }

  public function getSettings() {
    return $settings;
  }

  public function save() {
    ctools_include('export');
    ctools_export_crud_save('extra_field', $this);
  }

  public function delete() {
    ctools_include('export');
    ctools_export_crud_save('extra_field', $this);
  }

  /**
   * Implement a toString function to properly write the extra field on export
   * of attachments.
   */
  public function __toString() {
    return $this->name;
  }
}

class ExtraFieldAttachment implements ExtraFieldAttachmentInterface{
  var $extra_field;
  var $plugin;
  var $entity;
  var $bundle;
  var $type;
  var $settings = array();

  public function __construct($data = array()) {
    foreach ($data as $key => $val) {
      $this->$key = $val;
    }
  }

  public function setParentField(ExtraFieldInterface $parent) {
    $this->extra_field = $parent;
    $this->plugin = $parent->plugin;

    // Return ourself for chaining
    return $this;
  }

  public function setBundle($entity, $bundle) {
    $this->entity = $entity;
    $this->bundle = $bundle;

    // Return ourself for chaining
    return $this;
  }

  public function getFormSettingsForm() {}
  public function getViewSettingsForm() {}
  public function getFormDisplay(Object $entity) {}
  public function getViewDisplay(Object $entity) {}

  public function getSettings() {
    return $settings;
  }

  public function save() {
    ctools_include('export');
    ctools_export_crud_save('extra_field_attachment', $this);
  }

  public function delete() {
    ctools_include('export');
    ctools_export_crud_save('extra_field_attachment', $this);
  }
}
