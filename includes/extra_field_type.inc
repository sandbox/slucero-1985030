<?php
/**
 * @file
 * Class definitions for Extra Field Type plugins.
 */

/**
 * @todo Add documentation.
 */
interface ExtraFieldTypeInterface {
  /**
   * @todo Add documentation.
   */
  public function getFieldSettingsForm();

  /**
   * @todo Add documentation.
   */
  public function getAttachmentSettingsForm();

  /**
   * @todo Add documentation.
   */
  public function getFormDisplay();

  /**
   * @todo Add documentation.
   */
  public function getViewDisplay();
}

class ExtraFieldType implements ExtraFieldTypeInterface {
  var $plugin;

  public function __construct($data = array()) {
    $plugin = $data['plugin'];
  }

  /**
   * The field-level settings form for this plugin type.
   */
  public function getFieldSettingsForm() {
    $form = array();

    if (isset($plugin['field edit form']) && function_exists($plugin['field edit form'])) {
      $form = call_user_func($plugin['field edit form']);
    }

    return $form;
  }

  /**
   * The attachment-level settings form for this plugin type.
   */
  public function getAttachmentSettingsForm() {
    $form = array();

    if (isset($plugin['attachment edit form']) && function_exists($plugin['attachment edit form'])) {
      $form = call_user_func($plugin['attachment edit form']);
    }

    return $form;
  }

  /**
   * Display handler for the extra field type plugin.
   *
   * This handler should generate any display needed 
   * for the plugin in the form of a render array.
   */
  public function getFormDisplay($entity, $settings) {
    $display = array();

    if (isset($plugin['form display callback']) && function_exists($plugin['form display callback'])) {
      $display = call_user_func($plugin['form display callback'], $entity, $settings);
    }

    // @todo Allow altering of display content
    return $display;
  }

  /**
   * Display handler for the extra field type plugin.
   *
   * This handler should generate any display needed 
   * for the plugin in the form of a render array.
   */
  public function getViewDisplay($entity, $settings) {
    $display = array();

    if (isset($plugin['view display callback']) && function_exists($plugin['view display callback'])) {
      $display = call_user_func($plugin['view display callback'], $entity, $settings);
    }

    // @todo Allow altering of display content
    return $display;
  }
}
